search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).


%search_two(Elem,List)

search_two(X,[X,_,X|_]).
search_two(X,[_|Xs]) :- search_two(X,Xs).

%search_anytwo(Elem,List)
%search_anytwo(X,[X,X|_]).
search_anytwo(X,[_|Xs]) :- search_anytwo(X,Xs).
search_anytwo(X,[X|Xs]) :- search(X,Xs).

%sum(List,Sum)

sum([],0).
sum([Head | Tail], TotalSum) :-sum(Tail, Sum1),TotalSum is Head + Sum1.

%average(List,averagr)

average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C. 
average([X|Xs],C,S,A) :- C2 is C+1, S2 is S+X, average(Xs,C2,S2,A).

%maximum(List,Maximum)

maximum(List,M) :- maximum_(List,0,M).
maximum_([H|T],Now,Max) :-(H > Now -> maximum_(T,H,Max); maximum_(T,Now,Max)).
maximum_([],X,X).

same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

%all_bigger(List,List)
all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]) :- X > Y, all_bigger(Xs,Ys).

%sublist(List,List)
sublist([],List).
sublist([X|Xs],List) :- search(X,List), sublist(Xs,List).

seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

%seqR(Elem,List)
seqR(0,[0]).
seqR(N,[N|T]) :- N > 0, N2 is N-1,seqR(N2,T).

seqR2(N,List) :- seqR2_(N,List,0).
seqR2_(0,[I],I).
seqR2_(N,[I|T],I) :- N>0,N2 is N-1,I2 is I+1,seqR2_(N2,T,I2).

%inv(List,List)

concat([],L,L).
concat([H|T],L,[H|U]) :- concat(T,L,U).
inv([],[]).
inv([H|T],L):- inv(T,IT), concat(IT,[H],L).

%double(List,List)
double([],[]).
double(L1,L2) :- append(L1,L1,L2).

%times(List,N,List)
times(L1,N,L2) :- times(L1,N,0,L2,[]).
times(L1,N,N,L2,L2).
times(L,N,C,L2,L3) :- append(L,L3,L4), C2 is C+1, C2 =< N, times(L,N,C2,L2,L4).
  
%proj(List,List)
proj([],[]).
proj([[H|_]|Ls], [H|L]):- proj(Ls, L).
