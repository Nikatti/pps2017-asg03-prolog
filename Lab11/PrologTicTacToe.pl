%The two players
player('x').
player('o').
next_player('x','o').
next_player('o','x').

%Definition of a Tic-Tac-Toe Table
table([_,_,_,
       _,_,_,
       _,_,_]).


win([Z1,Z2,Z3,_,_,_,_,_,_],P):- Z1==P, Z2==P, Z3==P.
win([_,_,_,Z1,Z2,Z3,_,_,_],P):- Z1==P, Z2==P, Z3==P.
win([_,_,_,_,_,_,Z1,Z2,Z3],P):- Z1==P, Z2==P, Z3==P.
win([Z1,_,_,Z2,_,_,Z3,_,_],P):- Z1==P, Z2==P, Z3==P.
win([_,Z2,_,_,Z2,_,_,Z3,_],P):- Z1==P, Z2==P, Z3==P.
win([_,_,Z1,_,_,Z2,_,_,Z3],P):- Z1==P, Z2==P, Z3==P.
win([Z1,_,_,_,Z2,_,_,_,Z3],P):- Z1==P, Z2==P, Z3==P.
win([_,_,Z1,_,Z2,_,Z3,_,_],P):- Z1==P, Z2==P, Z3==P.

%All draw possibilities (the base one plus each possible rotations)
draw([Z1,_,Z2, _,_,Z3, _,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,_, _,_,Z2, Z3,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,_,Z1, Z2,_,_, _,Z3,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([Z1,_,_, _,_,Z2, Z3,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,_, Z2,_,_, Z3,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([Z1,_,Z2, Z3,_,_, _,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([Z1,Z2,_, _,_,Z3, Z4,_,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,Z2, Z3,_,_, _,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.

draw([Z1,_,_, _,Z2,Z3, Z4,_,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([Z1,_,Z2, _,Z3,_, _,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,_,Z1, Z3,Z3,_, _,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,_, _,Z2,_, Z3,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.

draw([Z1,_,_, _,Z2,Z3, _,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,_,Z1, Z2,Z3,_, _,Z4,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,_, Z2,Z3,_, _,_,Z4],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.
draw([_,Z1,_, _,Z2,Z3, Z4,_,_],P):- Z1 == P, Z2 == P, Z3 == P, Z4 == P.


mark(P,[X|_],1) :- var(X), X=P.
mark(P,[_,X|_],2) :- var(X), X=P.
mark(P,[_,_,X|_],3) :- var(X), X=P.
mark(P,[_,_,_,X|_],4) :- var(X), X=P.
mark(P,[_,_,_,_,X|_],5) :- var(X), X=P.
mark(P,[_,_,_,_,_,X|_],6) :- var(X), X=P.
mark(P,[_,_,_,_,_,_,X|_],7) :- var(X), X=P.
mark(P,[_,_,_,_,_,_,_,X|_],8) :- var(X), X=P.
mark(P,[_,_,_,_,_,_,_,_,X|_],9) :- var(X), X=P.

%Next/4 (@Table,@Player,-Result,-NewTable)
%next(Table,Player,Result,NewTable)
next(T,P,R,NT) :- mark(P,T,X),
                  NT = T,
                  (win(NT,P) -> R = 'Win'; draw(NT,P) -> R= 'Draw'; R = 'Nothing').

%game(@Table,@Player,-Result,-TableList)
%Output a list of tables resulting in a win for either player or a draw
%Note to self--use cut predicate to stop iterating a solution tree when a win condition is found

%game(T,P,R,TL):- next(T,P,R,NT),
%                 game(NT,X,R,[_,NT]).