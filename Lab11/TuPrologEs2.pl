% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%dropFirst(Elem,List,OutList)
dropFirst(X,[X|T],T):- !.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

%dropLast(Elem,List,OutList)                    
dropLast(X,[X|Xs],[H|L]):-dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).

%dropAll(Elem,List,OutList)
dropAll(X,[],[]).
dropAll(X,[X|Xs],L):- dropAll(X,Xs,L), !.
dropAll(X,[H|Xs],[H|L]):- dropAll(X,Xs,L).

dropAll2(X,[],[]).
dropAll2(X,[Y|Xs],L):- copy_term(X,Y), dropAll2(X,Xs,L), !.
dropAll2(X,[H|Xs],[H|L]):- dropAll2(X,Xs,L).

% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
fromCircList([H1],H,[e(H1,H)]).
fromCircList([H|T],G) :- fromCircList([H|T],H,G).
fromCircList([H1,H2|T],H,[e(H1,H2)|L]):- fromCircList([H2|T],H,L).

% dropNode(+Graph, +Node, -OutGraph)
dropNode(G,N,O):- dropAll2(e(N,_),G,G2),
dropAll2(e(_,N),G2,O).

% reaching(+Graph, +Node, -List)
reaching(G,Node,L) :- findall(X,member(e(Node,X),G),L).

% anypath(+Graph, +Node1, ?Node2, -ListPath)
anypath1(G,N1,N3,L) :- member(e(N1,N2),G), findall(L2,anypath1(G,N2,N3,L2),L3),append([e(N1,N2)],L3,L).

anypath(G,N1,N2,[e(N1,N2)]) :- member(e(N1,N2),G).
anypath(G,N1,N3,L) :- member(e(N1,N2),G), anypath(G,N2,N3,L2), append([e(N1,N2)],L2,L).

% allreaching(+Graph, +Node, -List)
allreaching(G,N,L) :- findall(N2,anypath(G,N,N2,_),L).